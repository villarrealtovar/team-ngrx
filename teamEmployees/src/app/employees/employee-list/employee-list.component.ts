import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Employee } from '../models/employee';
import { EmployeeService } from '../employee.service';
import { Store, select } from '@ngrx/store';

@Component({
  selector: 'team-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit, OnDestroy {
  pageTitle = 'Employees';
  errorMessage: string;
  displayTitle: boolean;
  employees: Employee[];
  selectedEmployee: Employee | null;
  sub: Subscription;

  constructor(private store:Store<any>,
            private employeeService: EmployeeService) { }

  ngOnInit() {
    this.sub = this.employeeService.selectedEmployeeChanges$.subscribe(
      selectedEmployee => this.selectedEmployee = selectedEmployee
    );

    this.employeeService.getEmployees().subscribe(
      (employees: Employee[]) => this.employees = employees,
      (err: any) => this.errorMessage = err.error,
      () => console.log('employeeService.getEmployees() completed')
    );

    //TODO: unsubscribe
    this.store.pipe(select('employees')).subscribe(
      employees => {
        if (employees) {
          this.displayTitle = employees.showEmployeeTitle;
        }
      }
    );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  checkChanged(value: boolean): void {
    this.store.dispatch({
      type:'TOOGLE_EMPLOYEE_TITLE',
      payload: value
    });
  }

  newEmployee(): void {
    //this.employeeService.changeSelectedEmployee(this.employeeService.newEmployee());
  }

  employeeSelected(employee: Employee): void {
    //this.employeeService.changeSelectedEmployee(employee);
  }

}
