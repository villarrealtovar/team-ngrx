export function reducer(state, action) {
    switch (action.type) {
        case 'TOOGLE_EMPLOYEE_TITLE':
            return {
                ...state,
                showEmployeeTitle: action.payload
            }
        default:
            return state;
    }
}