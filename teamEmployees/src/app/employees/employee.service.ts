import { Injectable } from '@angular/core';
import { Employee } from './models/employee';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, BehaviorSubject, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private employeesUrl = 'api/employees';
  private employees: Employee[];

  private selectedEmployeeSource = new BehaviorSubject<Employee | null>(null);
  selectedEmployeeChanges$ = this.selectedEmployeeSource.asObservable();
  constructor(private http: HttpClient) { }

  changeSelectedEmployee(selectedEmployee: Employee | null): void {
    this.selectedEmployeeSource.next(selectedEmployee);
  }

  getEmployees(): Observable<Employee[]> {
    if (this.employees) {
      return of(this.employees);
    }
    return this.http.get<Employee[]>(this.employeesUrl)
      .pipe(
        tap(data => console.log(JSON.stringify(data))),
        tap(data => this.employees = data),
        catchError(this.handleError)
      );
  }

  newEmployee(): Employee {
    return {
      id: 0,
      name: '',
      age: 0,
      username: '',
      hireDate: null,
      jobTitle: '',
      dateOfBirthday: null,
      country: '',
      status: false,
      area: '',
      tipRate: 0
    };
  }


  private handleError(err) {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An client error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }




  /** 
   TODO: Refactor Mario's Code

  getEmployee(id: number): Observable<any> {
    // const url = `${this.employeesUrl}/${id}`;
    // return this.http.get<Employee>(url);

    this.store.dispatch({
      type: employeeState.GET_EMPLOYEE,
      payload: { id: id }
    });
    return this.store.select('employees');
  }

  // PUT: update the employee on the server
  updateEmployee(employee: Employee): Observable<any> {
    // return this.http.put(this.employeesUrl, employee, httpOptions);
    this.store.dispatch({
      type: employeeState.SAVE_EMPLOYEE,
      payload: employee
    });
    return this.store.select('employees');
  }

  //POST: add a new employee to the server
  addEmployee(employee: Employee): Observable<any> {
    // return this.http.post<Employee>(this.employeesUrl, employee, httpOptions);
    this.store.dispatch({
      type: employeeState.ADD_EMPLOYEE,
      payload: employee
    });
    return this.store.select('employees');
  }

  // DELETE: delete the employee from the server
  deleteEmployee(employee: Employee | number): Observable<any> {
    const id = typeof employee === 'number' ? employee : employee.id;
    this.store.dispatch({
      type: employeeState.DELETE_EMPLOYEE,
      payload: { id: id }
    });
    return this.store.select('employees');
  } 
  
  */

  // /* GET Employees whose name contains search term */
  // searchEmployees(term: string): any {
  //   if (!term.trim()) {
  //     return of([]);
  //   }
  //   return this.http.get<Employee[]>(`${this.employeesUrl}/?name=${term}`);
  // }
  
}
