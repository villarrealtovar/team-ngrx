import { NgModule } from '@angular/core';
import { EmployeeMainComponent } from './employee-main/employee-main.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';

/** NgRx */
import { StoreModule } from '@ngrx/store';
import { reducer } from './models/employee.reducer';

const employeesRoutes: Routes = [
  { 
    path: 'main',
    component: EmployeeMainComponent
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(employeesRoutes),
    StoreModule.forFeature('employees', reducer)
  ],
  declarations: [
    EmployeeMainComponent,
    EmployeeListComponent,
    EmployeeEditComponent
  ]
})
export class EmployeesModule { }
