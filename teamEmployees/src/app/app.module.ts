import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserModule } from './user/user.module';
import { HomeModule } from 'src/app/home/home.module';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { EmployeeData } from './employees/employee-data';

/** NgRx */
import { StoreModule } from '@ngrx/store';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(EmployeeData),
    UserModule,
    HomeModule,
    AppRoutingModule,
    StoreModule.forRoot({})
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
