import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'team-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent {
  public pageTitle = 'Welcome';
}
