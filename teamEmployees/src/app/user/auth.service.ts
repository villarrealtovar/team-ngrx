import { Injectable } from '@angular/core';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser: User | null;
  redirectUrl: string;

  constructor() {  }

  isLoggedIn(): boolean {
      return !!this.currentUser;
  }

  login(userName: string, password: string): void {
      // TODO: This is just hard-coded.
      this.currentUser = {
          id: 2,
          userName: userName,
          isAdmin: false
      };
  }

  logout(): void {
      this.currentUser = null;
  }
}
